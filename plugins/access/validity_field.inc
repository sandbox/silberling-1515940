<?php

/**
 * @file
 * Plugin to provide access control/visibility based on a defined visibility schedule.
 */
$plugin = array(
  'title' => t('Validitiy Date Field'),
  'description' => t('Control access through a validity date field comparison.'),
  'callback' => 'validity_field_ctools_access_check',
  'default' => array('description' => '', 'field_name' => '', 'operation' => '', 'date_value' => ''),
  'settings form' => 'validity_field_ctools_access_settings',
  'settings form validate' => 'validity_field_ctools_access_settings_validate',
  'summary' => 'validity_field_ctools_access_summary',
  'required context' => new ctools_context_required(t('Context'), 'any'),
);

/**
 * Settings form
 */
function validity_field_ctools_access_settings($form, &$form_state, $conf) {
  $options = _validity_field_get_operation_options();

  $field_options = array();
  $fields = validity_field_default_fields();
  foreach ($fields as $field) {
    $field_config = $field['field_config'];
    $field_name = $field_config['field_name'];
    $field_options[$field_name] = t('Field') . ': ' . $field_name;
  }
  $form['settings']['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Administrative desc'),
    '#default_value' => $conf['description'],
    '#description' => t('A description for this validity test for administrative purposes.'),
  );
  $form['settings']['field_name'] = array(
    '#type' => 'select',
    '#options' => $field_options,
    '#title' => t('Date Field Name'),
    '#default_value' => $conf['field_name'],
    '#description' => t('Name of the validity field.'),
    '#required' => TRUE,
  );
  $form['settings']['operation'] = array(
    '#title' => t('Operation'),
    '#type' => 'select',
    '#options' => $options,
    '#description' => t('The comparison to perform to determine if the date field meets the condition. For multiple value date fields, all values will be checked to see if any meet the condition.'),
    '#default_value' => isset($conf['operation']) ? $conf['operation'] : '',
    '#required' => TRUE,
  );
  $form['settings']['date_value'] = array(
    '#title' => t('Value'),
    '#type' => 'textfield',
    '#description' => t("The value the field should contain to meet the condition. This can either be an absolute date in ISO format (YYYY-MM-DDTHH:MM:SS) or a relative string like '12AM today'. Examples: 2011-12-31T00:00:00, now, now +1 day, 12AM today, Monday next week. <a href=\"@relative_format\">More examples of relative date formats in the PHP documentation</a>.<br/>If Operation is 'Is valid today' or 'Is empty', this value will be ignored.", array('@relative_format' => 'http://www.php.net/manual/en/datetime.formats.relative.php')),
    '#default_value' => isset($conf['date_value']) ? $conf['date_value'] : '',
  );
  return $form;
}

/**
 * Implements hook_validate().
 * Validation for plugin settings form.
 */
function validity_field_ctools_access_settings_validate($form, &$form_state) {
  // Get field information from field name
  $field_info = field_info_field($form_state['values']['settings']['field_name']);
  if (!isset($field_info)) {
    form_set_error('settings][field_name', t('The field specified does not exist.'));
  }

  foreach (element_children($form) as $key) {
    if (!empty($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Check Field Value against configured operation and value
 */
function validity_field_ctools_access_check($conf, $context) {
  // If there is no context, disallow access
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  // Get settings
  $field_name = $conf['field_name'];
  $operation = $conf['operation'];
  $date_value = $conf['date_value'];

  // Retrieve entity data
  $entity = $context->data;
  $entity_type = $context->type[2];

  // Get all field items on context node
  $items = field_get_items($entity_type, $entity, $field_name);
  // If there are no values, nothing to do unless we were looking for 'empty' or '!='.
  if (empty($items)) {
    if ($operation == 'empty') {
      return TRUE;
    }
  }
  // All other operations need to retrieve the date values for comparision.
  else {
    $field = field_info_field($field_name);
    if (isset($field)) {
      $timezone_db = date_get_timezone_db($field['settings']['tz_handling']);
      foreach ($items as $delta => $item) {
        $timezone = date_get_timezone($field['settings']['tz_handling'], $item['timezone']);
        $date = new DateObject($item['value'], $timezone_db);
        date_timezone_set($date, timezone_open($timezone));
        $date1 = $date->format(DATE_FORMAT_DATETIME);
        $date = new DateObject($item['value2'], $timezone_db);
        date_timezone_set($date, timezone_open($timezone));
        $date2 = $date->format(DATE_FORMAT_DATETIME);
        if ($operation == 'now') {
          $date = date_create('today', date_default_timezone_object());
          $compdate = $date->format(DATE_FORMAT_DATETIME);
          $operation = '=';
        }
        else {
          str_replace('now', 'today', $date_value);
          $date = date_create($date_value, date_default_timezone_object());
          $compdate = $date->format(DATE_FORMAT_DATETIME);
        }

        switch ($operation) {
          case '=':
            if ($date2 >= $compdate && $date1 <= $compdate) {
              return TRUE;
            }
            break;
          case '>':
            if ($date1 > $compdate) {
              return TRUE;
            }
            break;
          case '<':
            if ($date2 < $compdate) {
              return TRUE;
            }
            break;
        }
      }
    }
  }
  return FALSE;
}

/**
 * Provide a summary description.
 */
function validity_field_ctools_access_summary($conf, $context) {
  $summary = !empty($conf['description']) ? check_plain($conf['description']) : t('No description');
  $summary .= ' (' . t('Field') . ' ' . $conf['field_name'] . ' ';
  $operation = $conf['operation'];
  $options = _validity_field_get_operation_options();
  $summary .= $options[$operation];
  if (($operation != 'now') && ($operation != 'empty')) {
    $summary .= ' ' . $conf['date_value'];
  }
  $summary .= ')';
  return $summary;
}

/*
 * Return a list of options for the operation field
 */
function _validity_field_get_operation_options() {
  return   array(
    '<' => t('Is in the past compared to'),
    '>' => t('Is in the future compared to'),
    '=' => t('Covers'),
    'now' => t('Is valid today'),
    'empty' => t('Is empty'),
  );
}