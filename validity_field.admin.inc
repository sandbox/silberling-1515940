<?php

/**
 * @file
 * Contains admin callbacks for the Validity Field module.
 */

/**
 * Form builder for the validity field form.
 */
function validity_field_apply_form($form, &$form_state) {
  // Get field definitions
  $fields = validity_field_default_fields();

  // Get all entity types
  $entity_types = entity_get_info();

  // pre-process all entity types
  foreach ($entity_types as $entity_type => $entity_info) {
    foreach ($entity_info['bundles'] as $bundle_type => $bundle_info) {
      $options[$entity_type . ':' . $bundle_type] = $entity_info['label'] . ': ' . $bundle_info['label'];
    }
  }

  $form['#fields'] = $fields;

  $field_options = array();

  foreach ($fields as $field) {
    $field_config = $field['field_config'];
    $field_name = $field_config['field_name'];
    $field_type = $field_config['type'];
    $field_info = field_info_field($field_name);
    $field_exists = isset($field_info);

    // If there is already a field, get a list of bundles it's attached to
    $default_bundles = array();
    if ($field_exists) {
      foreach ($field_info['bundles'] as $entity_type => $bundles) {
        foreach ($bundles as $bundle_type) {
          $default_bundles[] = $entity_type . ':' . $bundle_type;
        }
      }
    }

    $field_options[$field_name] = t('Field') . ': ' . $field_name;

    $form['bundles'] = array(
      '#title' => t('Bundles'),
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $default_bundles,
      '#description' => t('Select bundles on which to apply validity fields to.'),
      '#weight' => 5,
    );
    // Very neat but undocumented trick: see http://drupal.org/node/1349432
    foreach ($default_bundles as $option_key) {
      $form['bundles'][$option_key] = array(
        '#disabled' => TRUE,
      );
    }
  }

  $form['fields'] = array(
    '#title' => t('Field'),
    '#type' => 'checkboxes',
    '#options' => $field_options,
    '#required' => TRUE,
    '#description' => t('Select at least one validity field to be applied.'),
    '#weight' => 1,
  );
  module_load_include('inc', 'field_ui', 'field_ui.admin');
  $widget_options = field_ui_widget_type_options('date');
  $form['widget_type'] = array(
    '#type' => 'select',
    '#title' => t('Widget type'),
    '#required' => TRUE,
    '#options' => $widget_options,
    '#default_value' => $field_config['default_widget'],
    '#description' => t('The type of form element you would like to present to the user when creating the fields.'),
    '#weight' => 2,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create field and instances'),
    '#weight' => 10,
  );

  if (!isset($form['#theme'])) {
    $form['#theme'] = 'system_settings_form';
  }
  return $form;
}

/**
 * Submit handler for the validity field form.
 */
function validity_field_apply_form_submit($form, &$form_state) {
  $fields = $form['#fields'];

  foreach ($fields as $field) {
    $field_config = $field['field_config'];
    $field_name = $field_config['field_name'];
    $field_type = $field_config['type'];
    $field = field_info_field($field_name);
    $field_exists = isset($field);

    foreach (array_filter($form_state['values']['bundles']) as $option_key) {
      list($entity_type, $bundle_type) = explode(':', $option_key);

      // For existing fields Skip existing instances.
      if ($field_exists) {
        if (isset($field['bundles'][$entity_type]) && in_array($bundle_type, $field['bundles'][$entity_type])) {
          continue;
        }
      }
      else {
        field_create_field($field_config);
      }

      // Create a new field instance.
      $new_instance = array(
        'entity_type' => $entity_type,
        'bundle' => $bundle_type,
        'widget' => array(
          'type' => $form_state['values']['widget_type'],
        ),
      ) + $field_config;
      field_create_instance($new_instance);
      drupal_set_message(t('Field %field has been created on %entity_type:%bundle.', array(
                '%field' => $new_instance['label'],
                '%entity_type' => $entity_type,
                '%bundle' => $bundle_type,
              )));
    }
  }
}